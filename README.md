# CS316 Storefront

## Users Guru: Danielle
Danielle has shown strong engagement in lectures and discussions to prepare for the upcoming project.

## Products Guru: Charles
Charles has been actively generating product and style ideas that can be incorporated into the project.

## Carts Guru: Adi
Adi has been working on understanding how to connect databases and Python code for project implementation.

## Sellers Guru: Gage
Gage has actively participated in lectures and discussions while also contributing project ideas.

## Social Guru: Ashley
Ashley has been focusing on enhancing her SQL and database skills to contribute to the project.

---

